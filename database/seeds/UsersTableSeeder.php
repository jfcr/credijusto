<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'name'      =>  'Super Administrador',
            'email'     =>  'sa@sa.com',
            'password'  =>  Hash::make('Adm123Xyz'),
            'api_token' =>  Str::random(60)
        ]);
    }
}
