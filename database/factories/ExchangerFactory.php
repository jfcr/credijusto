<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Exchanger;
use Faker\Generator as Faker;

$factory->define(Exchanger::class, function (Faker $faker) {
    $providers  =   ['dof','fixer','banxico'];
    $values     =   [20.445,21.455,20.949,19.234];
    return [
        //
        'provider'  =>  $faker->randomElement($providers),
        'value'     =>  $faker->randomElement($values),
    ];
});
