<?php
declare(strict_types=1);

namespace App\Classes;
use GuzzleHttp\Client;

class Banxico implements ExchangerInterface
{

    use Rateable;
    private $url                = '';
    private $token              = '';

    public function setup(array $config): bool
    {
        if(!isset($config['url']) || !isset($config['token']) ){
            return false;
        }
        $this->url              =$config['url'];
        $this->token            =$config['token'];

        return true;

    }

    public function extract(): bool
    {

        return true;
        $client = new Client([
            'base_uri'  =>  $this->url,

        ]);
        //$client->setDefaultOption('verify', false);

        $x=[
            'curl' => [
                CURLOPT_SSL_VERIFYPEER      => false,
                CURLOPT_SSL_VERIFYHOST      =>  0,
                CURLOPT_SSL_VERIFYPEER      =>  0,
                CURLOPT_SSL_VERIFYSTATUS    =>  false
                //CURLOPT_SSLVERSION      =>  3,

            ]
        ];

        $response = $client->request('GET', '/?token=' .$this->token,$x);
        dd($response->getBody()->getContents());
    }







}