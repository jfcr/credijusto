<?php
declare(strict_types=1);

namespace App\Classes;
use Symfony\Component\DomCrawler\Crawler;
use GuzzleHttp\Client;



final class Dof implements ExchangerInterface
{
    use Rateable;
    private $url                = '';
    private $selector           = '';
    private $selector_text      = '';

    public function setup(array $config): bool
    {
        if(!isset($config['url']) || !isset($config['selector'])  || !isset($config['selector_text'])){
            return false;
        }
        $this->url              =$config['url'];
        $this->selector         =$config['selector'];
        $this->selector_text    =$config['selector_text'];
        return true;
    }

    public function extract(): bool
    {
        return true;
        $client = new Client([
            'base_uri'  =>  $this->url
        ]);
        $response = $client->request('GET', '/');
        file_put_contents('./txt.txt',$response->getBody()->getContents());

        $html=$response->getBody()->getContents();
        $crawler = new Crawler($html);
        $found = $crawler->filter($this->selector);
        dd($found->count());
    }


}