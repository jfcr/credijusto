<?php
declare(strict_types=1);

namespace App\Classes;


trait Rateable
{
    private $rate = -1;

    public function getRate():float{
        return $this->rate;
    }

    public function setRate(float $rate):bool{
        $this->rate = (float)$rate;
        return true;
    }

}