<?php
declare(strict_types=1);

namespace App\Classes;
use GuzzleHttp\Client;
use App\Models\Exchanger;


class Fixer implements ExchangerInterface
{
    use Rateable;
    private $url                = '';
    private $token              = '';

    public function setup(array $config): bool
    {
        if(!isset($config['url']) || !isset($config['token']) ){
            return false;
        }
        $this->url              =$config['url'];
        $this->token            =$config['token'];

        return true;
    }

    public function extract(): bool
    {
        $handle = curl_init();
        $url = $this->url.'?access_key='.$this->token.'&symbols=MXN';


        curl_setopt($handle, CURLOPT_URL, $url);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_HTTPHEADER, ['Content-type: application/json']);

        $data = curl_exec($handle);
        $response = json_decode((string)$data,true);

        if(isset($response['rates']) && isset($response['rates']['MXN'])) {
            $this->setRate($response['rates']['MXN']);
            return true;
        }

        return false;
    }


}