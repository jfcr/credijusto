<?php
declare(strict_types=1);

namespace App\Classes;


interface ExchangerInterface
{

    public function setup(array $config):bool;
    public function extract():bool;
    public function getRate():float;
    public function setRate(float $rate):bool;

}