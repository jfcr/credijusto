<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Classes\Fixer;
use App\Models\Exchanger;
use mysql_xdevapi\Exception;

class GetRates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'exchanger:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get rates from providers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Fixer

        $fixer = new Fixer();
        $config  =[
            'url'   =>  'http://data.fixer.io/api/latest',
            'token' =>  env('FIXER_KEY')
        ];
        $fixer->setup($config);
        $fixer->extract();

        try{
            if($fixer->getRate()<0){
                throw new \Exception("Check your FIXER_KEY env variable. Current value: ".env('FIXER_KEY'));
            }
            Exchanger::create([
                'provider'  =>  'fixer',
                'value'     =>  $fixer->getRate()
            ]);
        }catch (\Exception $exception){
            $this->error($exception->getMessage());
        }finally{
            $this->info('Task finished');
        }







    }
}
