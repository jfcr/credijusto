<?php

namespace App\Listeners;

use App\Events\ApiRequested;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;


class AddRate
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ApiRequested  $event
     * @return void
     */
    public function handle(ApiRequested $event)
    {
        //
        \Auth::user()->addRate();
    }
}
