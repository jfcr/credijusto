<?php

namespace App\Observers;

use App\Models\Exchanger;
//use App\Events\ApiRequested;

class ExchangerObserver
{
    /**
     * Handle the exchanger "created" event.
     *
     * @param  \App\Models\Exchanger  $exchanger
     * @return void
     */
    public function created(Exchanger $exchanger)
    {
        //

    }

    /**
     * Handle the exchanger "updated" event.
     *
     * @param  \App\Models\Exchanger  $exchanger
     * @return void
     */
    public function updated(Exchanger $exchanger)
    {
        //
    }

    /**
     * Handle the exchanger "deleted" event.
     *
     * @param  \App\Models\Exchanger  $exchanger
     * @return void
     */
    public function deleted(Exchanger $exchanger)
    {
        //
    }

    /**
     * Handle the exchanger "restored" event.
     *
     * @param  \App\Models\Exchanger  $exchanger
     * @return void
     */
    public function restored(Exchanger $exchanger)
    {
        //
    }

    /**
     * Handle the exchanger "force deleted" event.
     *
     * @param  \App\Models\Exchanger  $exchanger
     * @return void
     */
    public function forceDeleted(Exchanger $exchanger)
    {
        //
    }
}
