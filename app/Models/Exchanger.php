<?php
declare(strict_types=1);
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Exchanger extends Model
{
    //
    protected $guarded = ['created_at','updated_at'];
    protected $hidden=['id','updated_at'];

    public static function dof(string $date){
        $response = self::where('created_at',$date)->get();
        return $response->toArray();
    }
}
