<?php

namespace App\Http\Controllers\Api;

use App\Events\ApiRequested;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Exchanger;

class ExchangerController extends Controller
{
    //
    public function index(){
        $test = Exchanger::all();
        event(new ApiRequested($test));
        return response()->json(['message'=>$test]);
    }
}
