<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Classes\Dof;

class DofImplementationTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $dof =new Dof();
        $config = [
            'url'           =>  'http://www.banxico.org.mx/tipcamb/tipCamMIAction.do',
            'selector'      =>  'body > form > table > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(1) > td.b5 > table > tbody > tr:nth-child(3) > td:nth-child(3)',
            'selector_text' =>  'DOLAR'
        ];

        $this->assertTrue($dof->setup($config));

        $this->assertTrue($dof->extract());
    }
}
