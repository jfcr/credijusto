<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Classes\Fixer;

class FixerImplementationTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $fixer = new Fixer();
        $config  =[
            'url'   =>  'http://data.fixer.io/api/latest',
            'token' =>  'f0c42e97b04a8ac7ef7b009c6ce07c54'
        ];
        $fixer->setup($config);
        $this->assertTrue($fixer->extract());
    }
}
