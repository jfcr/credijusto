<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Classes\Banxico;

class BanxicoImplementationTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $banxico = new Banxico();
        $config  =[
            'url'   =>  'https://www.banxico.org.mx/SieAPIRest/service/v1/series/SF43787/datos/oportuno',
            'token' =>  '6dc30eba2b4cfb70b24e2a2087b8b1c9cc00b854974a5ae8c72226be7789d545'
        ];
        $banxico->setup($config);
        $banxico->extract();
        $this->assertTrue(true);
    }
}
