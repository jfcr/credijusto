<?php

namespace Tests\Unit;

use App\Models\School;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Exchanger;

class DatabaseTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testDatabase()
    {
        $date = now();
        factory(Exchanger::class)->create([
            'provider'      =>  'dof',
            'value'         =>  20,
            'created_at'    =>  $date
        ]);

        $dofs = Exchanger::dof($date);

        $this->assertEquals('dof',$dofs[0]['provider']);
        $this->assertEquals(20,$dofs[0]['value']);
    }
}
